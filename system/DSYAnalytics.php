<?php
/**
 * @package    DSYAnalytics
 *
 * @author     ivcha <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\Application\CMSApplication;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\Database\DatabaseDriver;

defined('_JEXEC') or die;

/**
 * DSYAnalytics plugin.
 *
 * @package  DSYAnalytics
 * @since    1.0
 */
class plgSystemDSYAnalytics extends JPlugin
{
	/**
	 * Application object
	 *
	 * @var    CMSApplication
	 * @since  1.0
	 */
	protected $app;

	/**
	 * Database object
	 *
	 * @var    DatabaseDriver
	 * @since  1.0
	 */
	protected $db;

	/**
	 * Affects constructor behavior. If true, language files will be loaded automatically.
	 *
	 * @var    boolean
	 * @since  1.0
	 */
	protected $autoloadLanguage = true;


	public function onAfterRoute()
    {
        //only going to run these in the frontend
        $app = JFactory::getApplication();
        if ($app->isClient("administrator")) {
            return;
        }
        // Get the current language code.
        $code = JFactory::getDocument()->getLanguage();

        // Get the new code.
        $propertyId = $this->params->get($code);

        if (strlen($propertyId) > 0){

            $script = $this->params->get('script');
            $script = str_replace("#ID", $propertyId, $script);

            $document = JFactory::getDocument();
            $options["version"]="auto";
            $attribs["async"] ="async";
            $document->addScript("https://www.googletagmanager.com/gtag/js?id=".$propertyId , $options , $attribs);

            $document->addScriptDeclaration($script);
        }
    }


    /**
     * Prepare form.
     *
     * @param   JForm  $form  The form to be altered.
     * @param   mixed  $data  The associated data for the form.
     *
     * @return  boolean
     *
     * @since	2.5
     */
    public function onContentPrepareForm($form, $data)
    {
        // Check we have a form.
        if (!($form instanceof JForm))
        {
            $this->_subject->setError('JERROR_NOT_A_FORM');

            return false;
        }

        // Check we are manipulating the languagecode plugin.
        if ($form->getName() !== 'com_plugins.plugin' || !$form->getField('propertyid', 'params'))
        {
            return true;
        }

        // Get site languages.
        if ($languages = JLanguageHelper::getKnownLanguages(JPATH_SITE))
        {
            // Inject fields into the form.
            foreach ($languages as $tag => $language)
            {
                $form->load('
                        <form>
                            <fields name="params">
                                <fieldset  name="basic" >
                                    <field
									name="' . strtolower($tag) . '"
									type="text"
									label="'.$language['name'].' ' .JText::_('PLG_SYSTEM_ANALYTCS_FIELD_LABEL') .'"
									description="' . JText::_('PLG_SYSTEM_ANALYTICS_FIELD_DESC') . '"
									translate_description="false"
									translate_label="false"
									size="20"
									filter="cmd"
								/>
							</fieldset>
						</fields>
					</form>
				');
            }
        }

        return true;
    }
}
